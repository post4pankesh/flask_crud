FROM python:3.6

COPY requirements.txt /usr/src/app/

RUN pip3 install -r /usr/src/app/requirements.txt
COPY . /usr/src/app

WORKDIR /usr/src/app

ENV port 5000
ENV APP_ENV development

EXPOSE $port

ENTRYPOINT ["python3","-u", "app.py"]
