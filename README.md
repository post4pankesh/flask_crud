# Flask CRUD application

This application is based on flask framework of python
It has the following methods :

```
GET      http://<host>:5000/get

POST/PUT http://<host>:5000/create

```


### example usage :    

```
POST  http://0.0.0.0:5000/create

reponse :

application/json
{
    "msg": "Hello world POST"
}
```

```
PUT  http://0.0.0.0:5000/create

reponse :

application/json
{
    "msg": "Hello world PUT"
}
```

## DOCKER USAGE

**Build the docker image**
```
docker build -t flask_crud:v1 .
```

**Run the docker image inside a container**
```
docker run -p 5000:5000 -t flask_crud:v1
```

