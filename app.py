from flask import Flask
from gevent.pywsgi import WSGIServer
from flask import request, Response
import json

app = Flask(__name__)


@app.route('/read')
def index():
    """
    THE
    :return:
    """
    return "Standard get request"


@app.route('/create',methods=["POST","PUT"])
def create():
    """
    This method is for CREATE
    accepts : PUT,POST
    :return:
    """
    if request.method == "POST":
        msg = {"msg": "Hello world POST"}
        msg = json.dumps(msg)
        return Response(status=200, response=msg, mimetype='application/json')

    elif request.method == "PUT":
        msg = {"msg": "Hello world PUT"}
        msg = json.dumps(msg)
        return Response(status=200, response=msg, mimetype='application/json')



if __name__ == "__main__":
    port = 5000
    http_server = WSGIServer(('0.0.0.0', port), app, None, 'default')
    http_server.serve_forever()